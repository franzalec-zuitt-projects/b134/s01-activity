1. List the books Authored by Marjorie Green.
    auId - 213-46-8915
    -BU1032 "The Busy Executives Database Guide"
    -BU2075 "You Can Combat Computer Stress!"

2. List the books Authored by Michael O'Leary.
    auId - 267-41-2394
    -BU1111 "Cooking with Computers"
    -TC7777 (Book or Title Name is not on the Title Table)


3. Write the author/s of "The Busy Executives Database Guide".
    - Marjorie Green
    - Abraham Bennet

4. Identify the publisher of "But is it User Friendly".
    - Algodata Infosystems

5. List the books published by Algodata Infosystems.
    - "But is it User Friendly"
    - "The Busy Executives Database Guide"
    - "Cooking with Computers"
    - "Straight Talk About Computers"
    - "Secrets of Silicon Valley"
    - "Net Etiquette"